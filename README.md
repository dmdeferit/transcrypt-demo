# transcrypt demo #

This repo is set up to demonstrate how [transcrypt](https://github.com/elasticdog/transcrypt) works.

In order to use transcrypt, you'll need the following installed:

  - bash 
  - git
  - openssl

# Usage

1. Clone this repo, and observe that `settings/production_secrets.py` [is encrypted](https://bitbucket.org/dmdeferit/transcrypt-demo/src/master/settings/production_secrets.py).
2. Initialise your local repo with the transcrypt password: 
   `./transcrypt -c aes-256-cbc -p 'SIYptCPvTS1ujy9ZZZFGPOKT6qLedh7uP1Q9zW6r'`
3. Observe that `settings/production_secrets.py` is now decrypted.

At this point, you can alter the secrets file (on new branch please), commit and push. 
Note that the file remains decrypted and viewable locally, but is automagically reencrypted at the origin.
